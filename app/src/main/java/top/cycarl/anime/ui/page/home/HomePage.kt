package top.cycarl.anime.ui.page.home

import android.graphics.Color
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import top.cycarl.anime.R
import top.cycarl.anime.ui.ext.fitWidthItem
import top.cycarl.anime.ui.page.RouteName
import top.cycarl.anime.ui.theme.Accent
import top.cycarl.anime.ui.theme.grey3
import top.cycarl.anime.ui.theme.grey300
import top.cycarl.anime.ui.theme.grey500

/**
 * @Author: cy
 * @date 2024/3/30 0030 16:25
 * @Version:1.0
 * @Description:
 **/
@Preview(backgroundColor = Color.WHITE * 1L, wallpaper = Color.WHITE)
@Composable
fun HomePage(
    modifier: Modifier = Modifier,
    navCtl: NavHostController = rememberNavController()
) {
    Column(modifier = Modifier.padding(horizontal = 12.dp)) {
        TopBar(navCtl = navCtl)
        HomeBody { dst ->
            navCtl.navigate(dst)
        }
    }
}

@Composable
fun TopBar(
    modifier: Modifier = Modifier,
    navCtl: NavHostController
) {
    Row(
        modifier = Modifier.height(66.dp), verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.img_avatar),
            contentDescription = "头像",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(32.dp)
                .clip(CircleShape)
        )
        Spacer(modifier = Modifier.padding(start = 15.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(28.dp)
                .background(color = grey300, shape = RoundedCornerShape(16.dp))
                .padding(start = 8.dp, end = 10.dp)
                .clickable {
                    navCtl.navigate("search/海猫鸣泣之时")
                }, verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_search),
                contentDescription = "搜索",
                modifier = Modifier.size(16.dp)
            )

            Text(
                text = "搜索你想看的吧~",
                textAlign = TextAlign.Start,
                fontSize = 12.sp,
                color = grey500
            )
        }

    }
}

@Composable
fun HomeBody(
    modifier: Modifier = Modifier,
    onNavigate: (String) -> Unit
) {
    LazyVerticalGrid(columns = GridCells.Fixed(3)) {
        fitWidthItem(key = "banner") {
            Image(
                painter = painterResource(id = R.drawable.img_banner),
                contentDescription = "轮播图",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .height(166.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }

        fitWidthItem(key = "label_favor") {
            LabelBar(
                label = "我的追番",
                modifier = Modifier.padding(top = 10.dp, bottom = 6.dp),
                onNavigate = onNavigate

            )
        }
        fitWidthItem(key = "label_hot") {
            LabelBar(
                label = "正在热播",
                modifier = Modifier.padding(top = 10.dp, bottom = 6.dp),
                onNavigate = onNavigate
            )
        }
        fitWidthItem(key = "label_timeline") {
            LabelBar(
                label = "新番时间表",
                modifier = Modifier.padding(top = 10.dp, bottom = 6.dp),
                onNavigate = onNavigate
            )
        }

        repeat(7) { i ->
            fitWidthItem(key = "line_$i") {
                TimelineBar()
            }
        }

        repeat(6) { i ->
            fitWidthItem(key = "label_body_$i") {
                LabelBar(
                    label = "今日热播",
                    modifier = Modifier.padding(top = 15.dp),
                    onNavigate = onNavigate
                )
            }
            repeat(12) { j ->
                item(key = "item_${i}_$j") {
                    AnimeCard()
                }
            }
        }
    }
}

@Composable
fun LabelBar(
    modifier: Modifier = Modifier,
    label: String = stringResource(id = R.string.label_favor),
    hasMore: Boolean = true,
    onNavigate: (String) -> Unit
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(text = label, fontSize = 14.sp)
        if (hasMore) {
            Box(modifier = Modifier
                .wrapContentWidth()
                .height(20.dp)
                .background(color = grey500, shape = RoundedCornerShape(10.dp))
                .clickable {
                    onNavigate(RouteName.CATEGORY)
                }
                .padding(horizontal = 4.dp), contentAlignment = Alignment.Center)
            {
                Text(
                    text = stringResource(id = R.string.label_see_more),
                    fontSize = 10.sp,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}


@Composable
fun TimelineBar(
    modifier: Modifier = Modifier,
    coverUrl: String = "",
    title: String = stringResource(id = R.string.title_yuki)
) {
    Row(
        modifier = Modifier
            .wrapContentHeight()
            .padding(vertical = 6.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.img_avatar),
            contentDescription = title,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(64.dp)
                .clip(RoundedCornerShape(8.dp))
        )

        Column(
            modifier = Modifier
                .padding(horizontal = 6.dp)
                .height(64.dp)
                .padding(vertical = 4.dp),
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                text = title,
                fontSize = 15.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                color = Accent
            )
            Text(
                text = "更新至12话",
                fontSize = 12.sp,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                color = grey3
            )
        }

    }
}

@Composable
fun AnimeCard(
    modifier: Modifier = Modifier,
    colorUrl: String = "",
    title: String = stringResource(id = R.string.title_yuki),
    status: String = "更新至12话"
) {
    Column(modifier = Modifier.padding(horizontal = 5.dp, vertical = 6.dp)) {
        Image(
            painter = painterResource(id = R.drawable.img_banner),
            contentDescription = "$title, $status",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .height(144.dp)
                .clip(RoundedCornerShape(6.dp))
        )

        Text(
            text = title,
            maxLines = 1,
            fontSize = 13.sp,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier.padding(top = 6.dp)
        )

        Text(
            text = status,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            fontSize = 10.sp,
            color = Accent,
            modifier = Modifier.padding(top = 3.dp)
        )

    }
}