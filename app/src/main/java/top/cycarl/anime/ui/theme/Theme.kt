package top.cycarl.anime.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val DarkColorScheme = darkColorScheme(
    primary = Purple80,
    secondary = PurpleGrey80,
    tertiary = Pink80
)

private val LightColorScheme = lightColorScheme(
    primary = Purple40,
    secondary = PurpleGrey40,
    tertiary = Pink40

    /* Other default colors to override
    background = Color(0xFFFFFBFE),
    surface = Color(0xFFFFFBFE),
    onPrimary = Color.White,
    onSecondary = Color.White,
    onTertiary = Color.White,
    onBackground = Color(0xFF1C1B1F),
    onSurface = Color(0xFF1C1B1F),
    */
)

@Composable
fun VegaTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}


private val DarkColorPalette = VegaColor(
    backgroundTop = black200,
    backgroundCenter = black100,
    backgroundBottom = black,
    backgroundEnd = black100,
    defaultIcon = grey,
    selectIcon = white,
    unselectIcon = grey,
    textTitle = white,
    textContent = grey,
    loginStart = black,
    loginEnd = black300,
    bottomBar = black,
    searchBar = black200,
    tabSelect = white,
    tabUnselect = grey,
    highlightColor = pink100
)

private val LightColorPalette = VegaColor(
    backgroundTop = white,
    backgroundCenter = white,
    backgroundBottom = pink100,
    backgroundEnd = pink200,
    defaultIcon = grey,
    selectIcon = pink100,
    unselectIcon = grey,
    textTitle = black,
    textContent = grey,
    loginStart = grey,
    loginEnd = pink100,
    bottomBar = white,
    searchBar = grey100,
    tabSelect = white,
    tabUnselect = black300,
    highlightColor = pink100
)


@Stable
class VegaColor(
    //由于是渐变色背景，所以背景由多个颜色组合而成
    backgroundTop: Color,//password page 上方背景颜色
    backgroundCenter: Color,//password page 中间背景颜色
    backgroundBottom: Color,//password page 底部背景颜色
    backgroundEnd: Color,
    defaultIcon: Color,// 默认图标填充颜色
    selectIcon: Color,// 图标被选中填充颜色
    unselectIcon: Color,//图标未选中填充颜色
    textTitle: Color,// 标题内容文字颜色
    textContent: Color,// 正文内容文字颜色
    loginStart: Color,//登录按钮前半部分背景颜色
    loginEnd: Color,//登录按钮后半部分背景颜色
    bottomBar: Color,
    searchBar: Color,
    tabSelect: Color,
    tabUnselect: Color,
    highlightColor: Color
){
    var backgroundTop: Color by mutableStateOf(backgroundTop)
        private set

    var backgroundCenter: Color by mutableStateOf(backgroundCenter)
        private set

    var backgroundBottom: Color by mutableStateOf(backgroundBottom)
        private set

    var backgroundEnd: Color by mutableStateOf(backgroundEnd)
        private set

    var defaultIcon: Color by mutableStateOf(defaultIcon)
        private set

    var selectIcon: Color by mutableStateOf(selectIcon)
        private set

    var unselectIcon: Color by mutableStateOf(unselectIcon)
        private set

    var textTitle: Color by mutableStateOf(textTitle)
        private set

    var textContent: Color by mutableStateOf(textContent)
        private set

    var loginStart: Color by mutableStateOf(loginStart)
        private set

    var loginEnd: Color by mutableStateOf(loginEnd)
        private set

    var bottomBar: Color by mutableStateOf(bottomBar)
        private set

    var searchBar: Color by mutableStateOf(searchBar)
        private set

    var tabSelect: Color by mutableStateOf(tabSelect)
        private set

    var tabUnselect: Color by mutableStateOf(tabUnselect)
        private set

    var highlightColor: Color by mutableStateOf(highlightColor)
        private set
}


private val LocalOwlColors = compositionLocalOf {
    LightColorPalette
}

object VegaTheme {
    val colors: VegaColor
        @Composable
        get() = LocalOwlColors.current

}

