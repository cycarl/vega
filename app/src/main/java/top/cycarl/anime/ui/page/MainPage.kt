package top.cycarl.anime.ui.page

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import kotlinx.coroutines.delay
import top.cycarl.anime.ui.theme.Purple80
import top.cycarl.anime.ui.theme.VegaTheme
import top.cycarl.anime.ui.theme.white

/**
 * @Author: cy
 * @date 2024/3/30 0030 15:33
 * @Version:1.0
 * @Description:
 **/

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainPage() {
    VegaTheme {
        ProvideWindowInsets {
            var isSplash by remember { mutableStateOf(true) }
            val systemUiController = rememberSystemUiController()
            SideEffect {
                systemUiController.setStatusBarColor(Color.Transparent, false)
            }
            if (isSplash) {
                SplashPage { isSplash = false }
            } else {
                AppScaffold()
            }
        }
    }
}

@Composable
fun SplashPage(block: () -> Unit) {
    var downCatch by remember { mutableStateOf(3) }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Purple80),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "Vega ${downCatch}",
            fontSize = 32.sp,
            color = white,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
//            modifier = Modifier.padding(0.dp, 150.dp, 0.dp, 0.dp)
        )

        LaunchedEffect(Unit) {
            while (downCatch >= 0) {
                delay(1000)
                downCatch--
            }
            block.invoke()
        }
    }
}