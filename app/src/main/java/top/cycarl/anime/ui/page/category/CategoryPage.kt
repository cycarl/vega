package top.cycarl.anime.ui.page.category

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import top.cycarl.anime.ui.theme.*

/**
 * @Author: cy
 * @date 2024/3/30 0030 20:48
 * @Version:1.0
 * @Description:
 **/

@OptIn(ExperimentalPagerApi::class)
@Preview()
@Composable
fun CategoryPage(
    modifier: Modifier = Modifier
) {
    val colors = listOf(green1, green2, green3, red2, red4, red8, yellow1)
    HorizontalPager(count = colors.size) { i ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = colors[i]),
            contentAlignment = Alignment.Center
        ) {
            Text(text = "category pager $i", fontSize = 24.sp, textAlign = TextAlign.Center)
        }
    }
}