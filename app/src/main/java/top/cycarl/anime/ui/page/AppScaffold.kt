package top.cycarl.anime.ui.page

import android.annotation.SuppressLint
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.add
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.navigationBars
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.windowInsetsBottomHeight
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.navigation.navigation
import top.cycarl.anime.R
import top.cycarl.anime.ui.page.category.CategoryPage
import top.cycarl.anime.ui.page.home.HomePage
import top.cycarl.anime.ui.page.mine.MinePage
import top.cycarl.anime.ui.page.rank.RankPage
import top.cycarl.anime.ui.page.search.SearchPage
import top.cycarl.anime.ui.theme.VegaTheme

/**
 * @Author: cy
 * @date 2024/3/30 0030 15:52
 * @Version:1.0
 * @Description:
 **/

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalMaterial3Api
@Composable
fun AppScaffold() {

    val navCtrl = rememberNavController()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        bottomBar = { BottomNavigationBar(navCtrl) },
    ) {
        NavigationGraph(
            modifier = Modifier.padding(it),
            navCtrl = navCtrl
        )
    }
}


@Composable
fun NavigationGraph(
    modifier: Modifier = Modifier,
    startPage: String = RouteName.MAIN,
    navCtrl: NavHostController
) {
    NavHost(
        navController = navCtrl,
        startDestination = startPage
    ) {

        navigation(startDestination = RouteName.HOME, route = RouteName.MAIN) {
            bottomNavPage(modifier, navCtrl)
        }

        composable(route = RouteName.CATEGORY) {
            CategoryPage()
        }

        composable(
            route = RouteName.SEARCH,
            arguments = listOf(navArgument("keyword") { type = NavType.StringType })
        ) { backStackEntry ->
            SearchPage(backStackEntry.arguments?.getString("keyword"))
        }

    }
}

fun NavGraphBuilder.bottomNavPage(
    modifier: Modifier = Modifier,
    navCtrl: NavHostController
) {
    composable(RouteName.HOME) {
        HomePage(modifier = modifier, navCtrl)
    }

    composable(RouteName.RANKING) {
        RankPage(modifier = modifier)
    }

    composable(RouteName.CATEGORY) {
        CategoryPage(modifier = modifier)
    }

    composable(RouteName.MINE) {
        MinePage(modifier = modifier)
    }
}


@Composable
fun BottomNavigationBar(
    navCtrl: NavHostController,
    menus: Array<NavMenu> = NavMenu.values()
) {
    val navBackStackEntry by navCtrl.currentBackStackEntryAsState()
    val destRoute = navBackStackEntry?.destination?.route
    if (destRoute !in menus.map { it.route }) {
        return
    }

    BottomNavigation(
        Modifier.windowInsetsBottomHeight(WindowInsets.navigationBars.add(WindowInsets(bottom = 56.dp))),
        backgroundColor = VegaTheme.colors.bottomBar
    ) {
        menus.forEach {
            BottomNavigationItem(
                selected = destRoute == it.route,
                icon = {
                    Icon(
                        painter = painterResource(id = it.icon),
                        contentDescription = it.route,
                        modifier = Modifier.size(20.dp)
                    )
                },
                label = { Text(text = stringResource(id = it.title), fontSize = 12.sp) },
                alwaysShowLabel = true,
                selectedContentColor = VegaTheme.colors.selectIcon,
                unselectedContentColor = VegaTheme.colors.unselectIcon,
                modifier = Modifier.navigationBarsPadding(),
                onClick = {
                    navCtrl.navigate(it.route) {
                        // 当用户选择子项时在返回栈中弹出到导航图中的起始目的地
                        // 来避免太过臃肿的目的地堆栈
                        navCtrl.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) { saveState = true }
                        }
                        // 当重复选择相同项时避免相同目的地的多重拷贝
                        launchSingleTop = true
                        // 当重复选择之前已经选择的项时恢复状态
                        restoreState = true
                    }
                }
            )
        }
    }
}

enum class NavMenu(
    @StringRes val title: Int,
    @DrawableRes val icon: Int,
    val route: String,
) {
    HomePage(R.string.text_home_page, R.drawable.icon_home, RouteName.HOME),
    RankPage(R.string.text_rank_page, R.drawable.icon_rank, RouteName.RANKING),
    CategoryPage(R.string.text_category_page, R.drawable.icon_search, RouteName.CATEGORY),
    MinePage(R.string.text_mine_page, R.drawable.icon_mine, RouteName.MINE)
}