package top.cycarl.anime.ui.page.search

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import top.cycarl.anime.App
import top.cycarl.anime.ui.theme.green1
import top.cycarl.anime.ui.theme.green2
import top.cycarl.anime.ui.theme.green3
import top.cycarl.anime.ui.theme.red2
import top.cycarl.anime.ui.theme.red4
import top.cycarl.anime.ui.theme.red8
import top.cycarl.anime.ui.theme.yellow1

/**
 * @Title: SearchPage
 * @Author: cy
 * @Date: 2024/4/8 18:31
 * @Description:
 */
private const val TAG = "SearchPage"

@OptIn(ExperimentalPagerApi::class)
@Preview()
@Composable
fun SearchPage(keyword: String? = null) {
    val colors = listOf(green1, green2, green3, red2, red4, red8, yellow1)
    App.toast("keyword: $keyword")
    HorizontalPager(count = colors.size) { i ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(color = colors[i]),
            contentAlignment = Alignment.Center
        ) {
            Text(text = "search pager $i", fontSize = 24.sp, textAlign = TextAlign.Center)
        }
    }
}