package top.cycarl.anime.ui.page.mine

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.sp

/**
 * @Title: MinePage
 * @Author: Lance
 * @Date: 2024/4/16 18:08
 * @Description:
 */
private const val TAG = "MinePage"

@Composable
fun MinePage(modifier: Modifier = Modifier) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(text = "我的", fontSize = 24.sp)
    }
}