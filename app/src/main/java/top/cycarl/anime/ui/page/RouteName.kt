package top.cycarl.anime.ui.page


object RouteName {
    const val HOME = "home"
    const val RANKING = "ranking"
    const val CATEGORY = "category"
    const val MINE = "mine"
    const val MAIN = "main"


    const val COLLECTION = "collection"
    const val PROFILE = "profile"
    const val SEARCH = "search/{keyword}"
    const val WEB_VIEW = "web_view"
    const val LOGIN = "login"
    const val ARTICLE_SEARCH = "article_search"

}