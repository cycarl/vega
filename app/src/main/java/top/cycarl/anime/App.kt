package top.cycarl.anime

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.widget.Toast
import dagger.hilt.android.HiltAndroidApp

/**
 * @Author: cy
 * @date 2024/3/30 0030 11:14
 * @Version:1.0
 * @Description:
 **/

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        context = this

    }


    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
            private set

        const val TAG = "App"

        @JvmStatic
        fun toast(msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }

    }

}